<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChildToCallsInComingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calls_in_comings', function (Blueprint $table) {
            
            $table->string('child_status')->nullable();
            $table->string('child_duration')->nullable();
            $table->string('child_call_status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calls_in_comings', function (Blueprint $table) {
            //
        });
    }
}
