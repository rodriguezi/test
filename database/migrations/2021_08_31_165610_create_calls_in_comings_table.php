<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallsInComingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls_in_comings', function (Blueprint $table) {
           
            $table->string('called',18)->nullable();
            $table->string('to_state',40)->nullable();
            $table->string('caller_country',50)->nullable();
            $table->string('direction',120)->nullable();
            $table->string('caller_state',50)->nullable();
            $table->mediumInteger('to_zip')->nullable();
            $table->string('call_sid',40)->nullable();
            $table->string('to',18)->nullable();
            $table->mediumInteger('caller_zip')->nullable();
            $table->string('to_conuntry',50)->nullable();
            $table->dateTime('api_version')->nullable();
            $table->mediumInteger('called_zip')->nullable();
            $table->string('call_status',20)->nullable();
            $table->string('called_city',40)->nullable();
            $table->string('from',18)->nullable();
            $table->string('account_sid',40)->nullable();
            $table->string('called_country',50)->nullable();
            $table->string('caller_city',40)->nullable();
            $table->string('caller',18)->nullable();
            $table->string('from_country',40)->nullable();
            $table->string('to_city',40)->nullable();
            $table->string('from_city',40)->nullable();
            $table->string('called_state',40)->nullable();
            $table->mediumInteger('from_zip',)->nullable();
            $table->string('from_state',40)->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls_in_comings');
    }
}
