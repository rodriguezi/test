<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController; 
use App\Http\Controllers\Firststep; 


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [PageController::class, 'inicio']);

Route::post('/calls', [PageController::class, 'callsIncoming']);

Route::post('/redirect', [PageController::class, 'redirect']);

Route::post('/firststep', [Firststep::class, 'firststep']);

Route::post('/secondstep', [Firststep::class, 'secondstep']);

Route::get('/sendcall', [Firststep::class, 'sendCall']); //     

Route::post('/listener', [Firststep::class, 'listener']);

Route::post('/listenerIn', [PageController::class,'listenerIn']);

Route::get('/record', [PageController::class,'record']);


