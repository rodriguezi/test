<?php

namespace App\Http\Controllers;

use App\Models\CallsInComing;
use Illuminate\Http\Request;


class PageController extends Controller
{
  // Retorna una vista 
  public function inicio(){
    return view('welcome'); 
  }

  //Muestra la DB en una view  
  public function record(){
    $calls = CallsInComing::all();
    return view('calls', compact('calls'));
  }

  // llegan las llamadas entrantes 
  public function callsIncoming(Request $request){

    //Hace el primer save 
    $call = new CallsInComing;

    $call->called = $request->Called;
    $call->to_state = $request->ToState;
    $call->caller_country = $request->CallerCountry;
    $call->direction = $request->Direction;
    $call->caller_state = $request->CallerState;
    $call->to_zip = $request->ToZip;
    $call->call_sid = $request->CallSid;
    $call->to = $request->To;
    $call->caller_zip = $request->CallerZip;
    $call->to_country = $request->ToCountry; // Problema en el nombre del campo 
    $call->api_version = $request->ApiVersion; // En duda 
    $call->called_zip = $request->CalledZip;
    $call->call_status = $request->CallStatus;
    $call->called_city = $request->CalledCity;
    $call->from = $request->From;
    $call->account_sid = $request->AccountSid;
    $call->called_country = $request->CalledCountry;
    $call->caller_city = $request->CallerCity;
    $call->caller = $request->Caller;
    $call->from_country = $request->FromCountry;
    $call->to_city = $request->ToCity;
    $call->from_city = $request->FromCity;
    $call->called_state = $request->CalledState;
    $call->from_zip = $request->FromZip;
    $call->from_state = $request->FromState;

    $call->save();



    // SPEECH

    setlocale(LC_TIME, "es_ES");
    define("CHARSET", "iso-8859-1");
    date_default_timezone_set('America/Mexico_City');
    $nowTime = time();
    $day_str = date('D', $nowTime);

    if ($nowTime <= strtotime("18:00") && $nowTime >= strtotime("10:00") && ($day_str == "Mon" || $day_str == 'Tue' || $day_str == 'Wed' || $day_str == 'Thu' || $day_str == 'Fri' || $day_str == 'Sat' || $day_str == 'Sun')) {
      $xml_str =  '<Response>' .
        '<Gather input="dtmf" timeout="15" numDigits="1" action="' . env('NGROK') . '/redirect">' .
        '<Say language="es-MX" loop="0">' .
        'Gracias por hablar a Empeña tu prenda' .
        '<break time="0.5s"/>' .
        'para empeños y ventas' .
        '<break time="0.5s"/>' .
        'marca 1,' .
        '<break time="1s"/>' .
        'para pago de refrendos e información de tu empeño' .
        '<break time="0.5s"/>' .
        'marca 2' .
        '<break time="1s"/>' .
        'si quieres hablar con un operador' .
        '<break time="1s"/>' .
        'marca 0' .
        '<break time="1s"/>' .
        '</Say>' .
        '</Gather>' .
        '</Response>';
    } else {
      $xml_str =  '<Response>' .
        '<Gather timeout="15" >' .
        '<Say language="es-MX" loop="0">' .
        'Gracias por hablar a Empeña tu prenda' .
        '<break time="0.5s"/>' .
        'en este momento no nos encontramos disponibles por este medio.<break time="0.5s"/> ' .
        'Por favor, envíanos un mensaje de güats app al siguiente número ' .
        '<break time="0.5s"/> símbolo de más <break time="0.5s"/> 1<break time="0.5s"/>58<break time="0.5s"/>67<break time="0.5s"/>85<break time="0.5s"/>57<break time="0.5s"/>52' .
        '<break time="0.5s"/> repito el número <break time="0.5s"/> símbolo de más <break time="0.5s"/> 1<break time="0.5s"/>58<break time="0.5s"/>67<break time="0.5s"/>85<break time="0.5s"/>57<break time="0.5s"/>52 ' .
        '<break time="0.5s"/>si prefieres comunicarte vía telefonica lo puedes hacer en un horario de lunes a viernes de 10 a eme a 6 pe eme' .
        '<break time="2s"/>' .
        '</Say>' .
        '</Gather>' .
        '</Response>';
    }

    $xml = new \SimpleXMLElement($xml_str);
    $xml = $xml->asXML();
    return response(str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="UTF-8"?>', $xml))->header('Content-Type', 'text/xml');
  }

  // redirecciona la lamada a SUPOR_AGENT y actualiza algunos datos
  public function redirect(Request $request){

    // Guarda la info del action
    $upDateCall = new CallsInComing;
    $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
      ->update([
        'digits' => $request->Digits,
        'call_status' => $request->CallStatus,
      ]);

    $xml_str =
      '<Response>' .
      '<Dial timeout="11" record="false">' .
      '<Client statusCallback="' . env('NGROK') . '/listener" statusCallbackEvent="completed">' .
      '<Identity>support_agent</Identity>' .
      '<Parameter name="origin" value="etp"/>' .
      '</Client>' .
      '</Dial>' .
      '</Response>';

    $xml = new \SimpleXMLElement($xml_str);
    $xml = $xml->asXML();
    return response(str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="UTF-8"?>', $xml))->header('Content-Type', 'text/xml');
  }
}
