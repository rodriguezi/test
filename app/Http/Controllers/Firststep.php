<?php

namespace App\Http\Controllers;

use App\Models\CallsInComing;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;

use function PHPUnit\Framework\isNan;

class Firststep extends Controller{

  //Realiza una llamada de salida y captura la  info 
  public function firststep(Request $request){

    // Realiza el primer guardado 
    $call = new CallsInComing;

    $call->called = $request->Called;
    $call->to_state = $request->ToState;
    $call->caller_country = $request->CallerCountry;
    $call->direction = $request->Direction;
    $call->caller_state = $request->CallerState;
    $call->to_zip = $request->ToZip;
    $call->call_sid = $request->CallSid;
    $call->to = $request->To;
    $call->caller_zip = $request->CallerZip;
    $call->to_country = $request->ToCountry; 
    $call->called_zip = $request->CalledZip;
    //$call->machine_detection_duration = $request->MachineDetectionDuration; // 'it could be enabled and added in the future'
    $call->call_status = $request->CallStatus;
    $call->called_city = $request->CalledCity;
    $call->from = $request->From;
    $call->account_sid = $request->AccountSid;
    $call->called_country = $request->CalledCountry;
    $call->caller_city = $request->CallerCity;
    $call->caller = $request->Caller;
    $call->from_country = $request->FromCountry;
    $call->to_city = $request->ToCity;
    $call->from_city = $request->FromCity;
    $call->called_state = $request->CalledState;
    $call->from_zip = $request->FromZip;
    //$call->answered_by = $request->AnsweredBy; // it could be enabled and added in the futurey
    $call->from_state = $request->FromState;

    $call->save();

    $xml_str =
      '<Response>' .
      '<Gather input="dtmf" timeout="10" numDigits="1" action="' . env('NGROK') . '/secondstep">' .
      '<Say language="es-MX" loop="0">' .
      '<break time="2s"/>' .
      'Hola ivan salgado' .
      '<break time="0.5s"/>' .
      'me comunico de empeña tu prenda! La oferta por tu artículo es por la cantidad de 1530 pesos.., repito la cantidad' .
      '<break time="0.5s"/>' .
      '1530 pesos,, si te interesa continuar o negociar, marca 1, de lo contrario finaliza la llamada' .
      '<break time="2s"/>' .
      '</Say>' .
      '</Gather>' .
      '</Response>';

    $xml = new \SimpleXMLElement($xml_str);
    $xml = $xml->asXML();
    return response(str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="UTF-8"?>', $xml))->header('Content-Type', 'text/xml');
  }

  
  //Redirige a nuestro panel y guarda el digito de la primer llamada 
  public function secondstep(Request $request){

    $upDateCall = new CallsInComing;
    $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
      ->update([
        'digits' => $request->Digits,
        //'finished_on_key' => $request->FinishedOnKey, // ADD finished_on_key
        //'machine_detection_duration' => $request->MachineDetectionDuration, // ADD machine_detection_duration
        'call_status' => $request->CallStatus,

      ]);


    $xml_str =
      '<Response>' .
      '<Dial timeout="11" record="false" callerId="+525535538833" callReason="Oferta ETPEMP027">' .
      '<Client statusCallback="' . env('NGROK') . '/listener" statusCallbackEvent="completed">support_agent</Client>' .
      '</Dial>' .
      '</Response>';

    $xml = new \SimpleXMLElement($xml_str);
    $xml = $xml->asXML();
    return response(str_replace('<?xml version="1.0"?>', '<?xml version="1.0" encoding="UTF-8"?>', $xml))->header('Content-Type', 'text/xml');


  }
  
  // Genera una llamada de salida
  
  public function sendCall(){

    $ngrok_url = env("NGROK");
    $ngrok_url = urlencode($ngrok_url);

    $to_num = "5620701358";

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://api.twilio.com/2010-04-01/Accounts/ACb69737dab394adb67533e01c74a1c5cd/Calls.json',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'Url=' . $ngrok_url . '%2Ffirststep&To=%2B52' . $to_num . '&From=%2B525570058266&StatusCallback=' . $ngrok_url . '%2Flistener&StatusCallbackEvent=initiated&StatusCallbackEvent=ringing&StatusCallbackEvent=answered&StatusCallbackEvent=completed&MachineDetection=Enable',
      CURLOPT_HTTPHEADER => array(
        'Authorization: Basic QUNiNjk3MzdkYWIzOTRhZGI2NzUzM2UwMWM3NGExYzVjZDozNjQ2OTQ1NjliNjhkMWMxOGQyOTg2OTZjZjVkOWU2NA==',
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    $response_arr = json_decode($response, true);
    /*
      // Guarda ciertos datos de la llamada de salida 
      $inCall = new CallsInComing;

      $inCall->call_sid = $response_arr["sid"];
      $inCall->to = $response_arr["to"];
      $inCall->api_version = $response_arr["api_version"];
      $inCall->call_status = $response_arr["status"];
      $inCall->from = $response_arr["from"];
      $inCall->account_sid = $response_arr["account_sid"];
      $inCall->save();
      
      */
       
    return response('<h1>Hola ' . env("NGROK") . ' </h1>')->header('Content-Type', 'text/html');
  }
  
  public function listener(Request $request){ //Toda actualizacion llega aqui 

    // Primer UpDate 

    $call_sid = ($request->ParentCallSid != "") ? $request->ParentCallSid : $request->CallSid;
    $upDateCall = CallsInComing::where('call_sid', $call_sid)->first();

    if ($upDateCall) {

      // CHILD
      //$upDateCall->called = $request->Called;
      //$upDateCall->parentCallSid = $request->ParentCallSid; 
      //$upDateCall->son_to = $request->To; // it could be enabled and added in the future
      //$upDateCall->child_duration = $request->CallDuration; 
      // $upDateCall->child_status = $request->CallStatus; 
      
      // Dad
      //$upDateCall->timestamp = $request->Timestamp; // ADD timestamp
      //$upDateCall->sip_response_code = $request->SipResponseCode;
      //$upDateCall->sequence_number = $request->SequenceNumber;
      $upDateCall->call_status = $request->CallStatus;

      //Segmenta llamadas entrantes y salientes
      if ($request->Direction == "inbound") {  //Ingresa a entrantes
        /*
        error_log('**********************');
        error_log('Entrante');
        error_log($upDateCall->child_status);
        error_log($upDateCall->digits);
        error_log('**********************');
        */
        //error_log($request);

        if ($upDateCall->digits == '' && $request->CallDuration >= 18){ // Escucho todo el menu pero colgo FUNCIONA
  
          $endStatus = 'HUNG_UP_NO_ANSWER';
          $upDateCall->end_status = $endStatus;

        }else if ($upDateCall->child_status == "no-answer"){ // Intento comunicarse NO OBTUVO RESP 

            $endStatus = 'THE_CALL_WAS_NOW_ASWERED';
            $upDateCall->end_status = $endStatus;

        }else if ($upDateCall->digits == !null && $request->CallStatus = 'completed'){ // Llamada exitosa

            $endStatus = 'SUCCESSFUL_CALL';
            $upDateCall->end_status = $endStatus;
            error_log('logramos el seguimiento');
        }
      }
      else if ($request->Direction == "outbound-api" || $request->Direction == "outbound-dial") {  //Salientes


        //Ingresa a salientes
        if ($request->ParentCallSid == "" || $request->ParentCallSid == null) { // First Call (Dad)
          if ($request->AnsweredBy == 'machine_start' || $request->CallStatus == 'failed') { // DETECT MACHINE 'Rechazo o no contesto' ,FUNCIONA 
            $endStatus = 'MACHINE_ANSWER_OR_FAILED';
            $upDateCall->end_status = $endStatus;
          } else if ($request->AnsweredBy == 'human' || $request->AnsweredBy == 'fax' || $request->AnsweredBy == 'unknown') { // Si contesta un humano o algo no detectable 
            if ($request->CallDuration >= 24 && $request->CallStatus == 'completed' && $upDateCall->digits == "" && $request->MachineDetectionDuration < 1) { // Case colgo sin respuesta ,FUNCIONANDO 
              $endStatus = 'HUNG_UP_NO_ANSWER';
              $upDateCall->end_status = $endStatus;
            } else if ($request->CallDuration <= 24 && $request->CallStatus == 'completed' && $upDateCall->digits == "") { // Case 'Colgo de inmediato' , FUNCIONANDO
              $endStatus = 'HANG_UP';
              $upDateCall->end_status = $endStatus;
            }
          }
        }
        else if ($request->ParentCallSid != "") { //Second Call (child)
          
          $upDateCall->child_status = $request->CallStatus; // Actualizara el estado en caso de que le llegue un "NO ANSWER"
          
          error_log($upDateCall->CallStatus);
          error_log($upDateCall->digits);
          error_log($upDateCall->child_status);

          if ($request->CallStatus == 'completed' && $upDateCall->digits == !null && $upDateCall->child_status == 'completed' ) { // Nos comunicamos con exito FUNCIONANDO

            $endStatus = 'SUCCESSFUL_CALL';
            $upDateCall->end_status = $endStatus;

          } else if ($upDateCall->digits == !null && $upDateCall->child_status == 'no-answer') { // Intento comunicarse nadie respondio // FUNCIONANDO

            $upDateCall->call_status_child = $request->CallStatus;

            $endStatus = 'SUCCESSFUL_CALL';
            $upDateCall->end_status = $endStatus;

          }

        }
      }
      error_log('FIN');
      $upDateCall->save();
    } //End if iniated
  }
  
}










  /* 
       
        $calls = CallsInComing::where('call_sid', $request->CallSid)->first();

        // error_log($calls->digits);
        // error_log($calls->call_status);
        // error_log($calls);

        error_log('===========================1==================================');
        if($calls){
          error_log('===========================Find it==================================');

          if ($calls->from == "+525570058266"){

            error_log('===========================2==================================');
  
            error_log('Saliente:_'.$calls->digits.'_');
  
            if ($request->CallDuration <= 24 && $request->CallStatus =='completed' && $calls->digits == ""){
             
              $endStatus = 'Colgo de inmediato';
              error_log($endStatus); 
             
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
                  'end_status' => $endStatus,
              
              ]); 
            }else if ($request->CallDuration >= 24 && $request->CallStatus =='completed' && $calls->digits == "" && $calls->detection != 'machine_start' && $request->MachineDetectionDuration < 1){
              
              
              $endStatus = 'Escucho todo el menu pero no acepto la oferta';
              error_log($endStatus); 
             
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
              'end_status' => $endStatus,
              
               ]); 
            }else if($request->CallDuration >= 24 && $request->CallStatus =='completed' && $calls->digits == !null && $calls->call_status == 'no-answer'){
              
              
              $endStatus = 'escucho todo el menu y se intento comunicar SIN RESPUESTA';
              error_log($calls -> digits);
              error_log('Fue su numero');
              error_log($endStatus); 
             
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
              'end_status' => $endStatus,
              
                ]); 
            }else if($request->CallDuration >= 29 && $request->CallStatus =='completed' && $calls->digits == !null && $request->CallStatus == 'completed' ){
  
              $endStatus = 'Nos comunicamos con exito';
              error_log($calls -> digits);
              error_log('Fue su numero');
              error_log($endStatus); 
             
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
              'end_status' => $endStatus,
              
                ]);
            }else if($request->AnsweredBy== 'machine_start' || $request->CallStatus =='failed' ){
  
              error_log('Ocupado o rechazado');
  
              $endStatus = 'BUSY_REJECTED_FAILED';
              error_log($endStatus); 
             
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
              'end_status' => $endStatus,
              
                  ]);
            }
            error_log('===========================1.1==================================');
  
          }else{
            error_log('===========================3==================================');
  
            error_log('ENTRANTE');
            if ($calls->digits == null && $request->CallDuration >= 18){
  
  
              $endStatus = 'Escucho todo el menu pero no realizo ninguna interaccion';
              error_log($endStatus); 
              
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
              'end_status' => $endStatus,
              
              ]);
  
            }else if ($calls->call_status == "no-answer" &&  $calls->digits == !null){
  
              error_log($calls->digits);
  
              $endStatus = 'Intento comincarse NO SE ATENDIO LA LLAMADA';
              error_log($endStatus); 
              
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
              'end_status' => $endStatus,
              
              ]);
            }else if ($calls->digits == !null && $calls->call_status != 'no-answer'){
  
  
              error_log($calls->digits);
  
              $endStatus = 'Nos comunicamos con exito';
              error_log($endStatus); 
              
              $upDateCall = CallsInComing::where('call_sid', $request->CallSid)
              ->update([
  
              'end_status' => $endStatus,
              
              ]);
  
            }
            error_log('===========================3.1==================================');
  
          }
        }else{
          error_log('===========================Dont find it==================================');
        }
       
        error_log('===========================4==================================');

        return response()->json(['status' => '200', 'message' => 'Procesado con exito']);
      */

